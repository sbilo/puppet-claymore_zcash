class claymore_zcash::service inherits claymore_zcash {

  file { '/lib/systemd/system/claymore-zcash.service':
    ensure  => file,
    content => template('claymore_zcash/claymore-zcash.service.erb'),
    notify  => Service['claymore-zcash']
  }

  service { 'claymore-zcash':
    ensure => $claymore_zcash::enabled ? {
      true  => running,
      false => stopped,
    },
    enable => $claymore_zcash::enabled
  }
}