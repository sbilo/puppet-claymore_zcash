class claymore_zcash::config inherits claymore_zcash {

  file { "${claymore_zcash::install_dir}/epools.txt":
    ensure  => file,
    content => template('claymore_zcash/epools.txt.erb'),
    notify  => Service['claymore-zcash']
  }
}