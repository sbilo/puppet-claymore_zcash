class claymore_zcash::install inherits claymore_zcash {

  $source = "https://github.com/nanopool/ClaymoreZECMiner/releases/download/v${claymore_zcash::version}/Claymore.s.ZCash.AMD.GPU.Miner.v${claymore_zcash::version}.-.LINUX.tar.gz";

  $filename = basename($source);

  archive { "${claymore_zcash::install_dir}/${filename}":
    ensure        => present,
    extract       => true,
    extract_path  => $claymore_zcash::install_dir,
    source        => $source
  }
}