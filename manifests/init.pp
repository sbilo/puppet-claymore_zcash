# Class: claymore_zcash
# ===========================
#
# Full description of class claymore_zcash here.
#
# Parameters
# ----------
#
# @param version
# @param env_vars
# @install_dir
# @target_temp
# @ethereum_pools
# @ethereum_wallet
# @dual_pools
# @dual_wallet
# @zcash_pools
# @zcash_wallet
#
# Authors
# -------
#
# Author Name <sander.bilo@gmail.com>
#
# Copyright
# ---------
#
# Copyright 2017 Sander Bilo, unless otherwise noted.
#
class claymore_zcash(
  String $version                   = '12.6',
  Array[String] $env_vars           = [
    'GPU_FORCE_64BIT_PTR=0',
    'GPU_MAX_HEAP_SIZE=100',
    'GPU_USE_SYNC_OBJECTS=1',
    'GPU_MAX_ALLOC_PERCENT=100',
    'GPU_SINGLE_ALLOC_PERCENT=100'],
  Integer $target_temp              = 55,
  Boolean $enabled                  = true,
  Stdlib::AbsolutePath $install_dir = '/opt/claymore-zcash',
  Boolean $enable_watchdog          = true,
  Optional[Integer] $allpools       = 1,
  Optional[String] $email           = 'sander.bilo@gmail.com',
  Optional[String] $zpasswd         = 'x',
  Optional[Array[String]] $zpools   = ['zec-eu.suprnova.cc:2142', 'zec.suprnova.cc:2142','zec-eu1.nanopool.org','zec-eu2.nanopool.org'],
  Optional[String] $zwallet         = 't1SVc4hVNx5BQHXfeup7JwCM3XYGhBW3D9e'
) {

  contain claymore_zcash::install
  contain claymore_zcash::config
  contain claymore_zcash::service

  Class['::claymore_zcash::install']
  -> Class['::claymore_zcash::config']
  ~> Class['::claymore_zcash::service']
}
